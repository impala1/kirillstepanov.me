#!/bin/sh

ROOT_PATH=$(realpath "$(dirname $(realpath $0))/..")
SRC_PATH=$(realpath "${ROOT_PATH}/src")
HOST=$1

msg()
{
    echo ">>> $@"
}

do_deploy()
{
    msg "Copying files..."
    cd ${SRC_PATH}
    rsync \
        -avz \
        --delete \
        --exclude=*.scss \
        --exclude=*.ls \
        --exclude=*.json \
        --exclude=*.less \
        --exclude=*.map \
        --exclude=*src* \
        . \
        ${HOST}:

    # https://members.nearlyfreespeech.net/wiki/HowTo/GzipStatic
    ssh ${HOST} '../protected/gzip.pl'

    msg "Done"
}

if ! type rsync > /dev/null; then
    msg "Command 'rsync' is not installed. Cannot proceed."
    exit 1
fi

if [ -z ${HOST} ]; then
    msg 'Hostname not provided'
else
    do_deploy
fi
