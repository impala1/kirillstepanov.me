#!/bin/sh

ROOT_PATH=$(realpath "$(dirname $(realpath $0))/..")
SRC_PATH=$(realpath "${ROOT_PATH}/src")
LS_SRC="${SRC_PATH}/static/js"
SASS_SRC="${SRC_PATH}/static/css"

msg()
{
    echo ">>> $@"
}

compile_ls()
{
    msg "Compiling Livescript file(s)"
    find $LS_SRC -name *.ls -exec lsc -c {} \;
    msg "Done"
}

compile_sass()
{
    msg "Compiling SASS file(s)"
    # there must be a better way!
    find $SASS_SRC -name *.scss -exec bash -c 'sass --scss {} $(dirname {})/$(basename -s scss {})css' \;
    msg "Done"
}

watch_ls()
{
    msg "Autocompiling Livescript file(s)..."
    lsc -c -w ${LS_SRC}
}

watch_sass()
{
    compile_sass
    msg "Autocompiling SASS file(s)..."
    sass --watch ${SASS_SRC}
}


if ! type lsc > /dev/null; then
    msg "Command 'lsc' is not installed. Cannot proceed."
    exit 1
fi

if ! type sass > /dev/null; then
    msg "Command 'sass' is not installed. Cannot proceed."
    exit 1
fi

case $1 in
    "watch")
        watch_ls &
        watch_sass
    ;;
    *)
        compile_ls
        compile_sass
    ;;
esac
