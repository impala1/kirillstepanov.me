# http://stackoverflow.com/a/10142256
# https://stackexchange.com/legal - Subscriber Content
# BEGIN CC-BY-CA 3.0
Array.prototype.shuffle = ->
    i = this.length
    j = void
    temp = void

    while ( --i > 0)
        j = Math.floor( Math.random() * ( i + 1 ) )
        temp = this[i]
        this[i] = this[j]
        this[j] = temp

    this
# END CC-BY-CA 3.0

delay = 50

animate_text = (container, init_delay = 0)->
    init_delay_multiplier = 2
    container.html ["<span class='letter invisible'>#x</span>" for x in container.html() when x].join ''

    container.children().each (index) ->
        elem = $(this)
        setTimeout ->
            elem.removeClass('invisible')
        , delay * (index + init_delay_multiplier) + init_delay

    (container.children().length + init_delay_multiplier) * delay


do_text_animation = ->
    subtitle_delay = animate_text $('#title')
    animate_text $('#subtitle'), subtitle_delay


do_bg_fill = (terms) ->
    num_lines = 25
    bg = $ '#bg'
    elem = $ '<p class="text" />'

    gen_line = ->
        elem.clone().html terms.shuffle().join ' '

    [bg.append gen_line() for i to num_lines]


do_scroller_animation = (terms) ->
    display_time = 2500
    animation_duration = 500
    offset = 100

    elem = $ '#scroller'
    display = elem.find '.insert'
    terms = [$("<span class='term above'>#x</span>") for x in terms].shuffle()
    display.html terms[0].removeClass 'above'
    i = 1

    next = ->
        current = display.find '.term'
        next = terms[i++ % terms.length]
        current.addClass 'below'
        display.prepend next

        setTimeout ->
            next.removeClass 'above'
        , offset

        setTimeout ->
            current.remove().removeClass('below').addClass 'above'
        , animation_duration + offset

    setInterval ->
        next()
    , display_time


$(document).ready ->
    terms = [x for x in $('#terms').text().split /\s*[\r\n,]+\s*/ when x]

    do_text_animation()
    do_bg_fill([x for x in terms]) # clone list
    do_scroller_animation([x for x in terms]) #clone list
